/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import jade.core.Profile;
import jade.core.ProfileImpl;
import javax.swing.UIManager;
import jade.core.Runtime;
import jade.wrapper.*;
/**
 *
 * @author NoHearth
 */
public class SmartAgent 
{
    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e)
        {}
        
        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();
        AgentContainer mc = runtime.createMainContainer(profile);
        try
        {
            AgentController ac = mc.createNewAgent("rma","jade.tools.rma.rma", null);
            ac.start();
            ac = mc.createNewAgent("Smart-Delivery", "agentes.AgenciaRandom", null);
            ac.start();
            for(int i=0;i<3;i++)
            {
                ac = mc.createNewAgent("Pedido-"+(i+1), "agentes.PedidoRandom", null);
                ac.start();
            }
        } catch (StaleProxyException e)
        {}
    }
}
