/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import modelos.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author NoHearth
 */
public class VEdiPedido extends JDialog
{
    private Container c;
    private JLabel lblnom;
    private JLabel lblcll;
    private JLabel lblcrr;
    private JButton btnok;
    private JButton btncan;
    private JTextField txtnom;
    private JTextField txtcll;
    private JTextField txtcrr;
    private Direccion direccion;
    private ModelTabla tabla;
    
    public VEdiPedido(JFrame parent, String titulo, boolean modal, Direccion dir)
    {
        super(parent, titulo, modal);
        direccion = dir;
        c = new Container();
        lblnom = new JLabel("Nombre");
        lblcll = new JLabel("Calle");
        lblcrr = new JLabel("Carrera");
        btnok = new JButton("Aceptar");
        btncan = new JButton("Cancelar");
        txtnom = new JTextField();
        txtcll = new JTextField();
        txtcrr = new JTextField();
        tabla =  new ModelTabla(new ArrayList<DatosPedido>());
        
        setBounds(100, 100, 232, 200);
        setLocationRelativeTo(null);
        setResizable(false);
        
        c = this.getContentPane();
        c.setLayout(null);
        c.setBackground(new Color(102, 102, 102));
        
        lblnom.setBounds(20,25,43,22);
        lblnom.setFont(new Font("Cambria", 0, 12));
        lblcll.setBounds(20,55,27,22);
        lblcll.setFont(new Font("Cambria", 0, 12));
        lblcrr.setBounds(20,85,43,22);
        lblcrr.setFont(new Font("Cambria", 0, 12));
        
        txtnom.setBounds(70,25,120,20);
        txtnom.setFont(new Font("Cambria", 0, 12));
        txtcll.setBounds(150,55,40,20);
        txtcll.setFont(new Font("Cambria", 0, 12));
        txtcrr.setBounds(150,85,40,20);
        txtcrr.setFont(new Font("Cambria", 0, 12));
        
        btnok.setBounds(24, 115, 82, 28);
        btnok.setFont(new Font("Cambria", 1, 12));
        btnok.setBackground(new Color(102, 102, 102));
        
        btncan.setBounds(126, 115, 82, 28);
        btncan.setFont(new Font("Cambria", 1, 12));
        btncan.setBackground(new Color(102, 102, 102));
        
        c.add(lblnom);
        c.add(lblcll);
        c.add(lblcrr);
        c.add(txtnom);
        c.add(txtcll);
        c.add(txtcrr);
        c.add(btnok);
        c.add(btncan);
        
        if(direccion!=null)
        {
            txtnom.setText(direccion.getNombre());
            txtcll.setText(String.valueOf(direccion.getNumCalle()));
            txtcrr.setText(String.valueOf(direccion.getNumCarrera()));
        }
        btncan.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent arg0) {
                dispose();
            }
        });
        
        btnok.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent arg0) {
                if(validarCampos())
                {
                    direccion = new Direccion(txtnom.getText().trim(),
                                              Integer.parseInt(txtcll.getText().trim()),
                                              Integer.parseInt(txtcrr.getText().trim()));
                    //tabla.actualizar_pedido(row, direccion);
                    dispose();
                }else
                    aviso("Campos Invalidos", "Advertencia");
            }
        });
    }
    
    private boolean validarCampos(){
        if(txtnom.getText().trim().equals("") || txtcll.getText().trim().equals("") || txtcrr.getText().trim().equals(""))
            return false;
        else
            return true;
    }
    
    public void aviso(String mensaje, String titulo) 
    {
        JOptionPane.showMessageDialog(this, mensaje, titulo, 2);
    }
    
    public Direccion mostrar() {
        setVisible(true);
        return direccion;
    }
}
