/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import agentes.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import modelos.Direccion;
/**
 *
 * @author NoHearth
 */
public class VAgenciaRandom extends JFrame
{
    private AgenciaRandom agente;
    private JButton btnmtd1;
    private JLabel lbldirec;
    private JLabel lblcall;
    private JLabel lblcarr;
    private JLabel lbllgr;
    private JTextField txtcall;
    private JTextField txtcarr;
    private JTextField txtlgr;
    private Container c;
    
    int init = 0;
 
    public VAgenciaRandom(AgenciaRandom agencia)
    {
        agente = agencia;
        btnmtd1 = new JButton("Buscar Rutas");
        lbldirec = new JLabel("Direccion de la tienda");
        lblcall = new JLabel("Calle");
        lblcarr = new JLabel("Carrera");
        lbllgr = new JLabel("Lugar");
        txtcall = new JTextField();
        txtcarr = new JTextField();
        txtlgr = new JTextField();
        c = new Container(); 
        
        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) {
                agente.doDelete();
            }
        });
        setBounds(100, 100, 360, 200);
        setLocationRelativeTo(null);
        setResizable(false);
        c = this.getContentPane();
        c.setLayout(null);
        c.setBackground(new Color(102, 102, 102));
        this.setTitle(agencia.getLocalName());
 
        lbldirec.setBounds(20, 20, 121, 12);
        lbldirec.setFont(new Font("Cambria", 1, 12));
        lbllgr.setBounds(30,50,30,22);
        lbllgr.setFont(new Font("Cambria", 0, 12));
        lblcall.setBounds(30,80,27,22);
        lblcall.setFont(new Font("Cambria", 0, 12));
        lblcarr.setBounds(30,110,43,22);
        lblcarr.setFont(new Font("Cambria", 0, 12));
        
        txtlgr.setBounds(77,50,120,20);
        txtlgr.setFont(new Font("Cambria", 0, 12));
        txtcall.setBounds(157,80,40,20);
        txtcall.setFont(new Font("Cambria", 0, 12));
        txtcarr.setBounds(157,110,40,20);
        txtcarr.setFont(new Font("Cambria", 0, 12));
        
        btnmtd1.setBounds(220, 50, 120, 28);
        btnmtd1.setFont(new Font("Cambria", 1, 12));
        btnmtd1.setBackground(new Color(102, 102, 102));
        
        c.add(lbldirec);
        c.add(lblcall);
        c.add(lblcarr);
        c.add(lbllgr);
        c.add(txtcall);
        c.add(txtcarr);
        c.add(txtlgr);
        c.add(btnmtd1);

        btnmtd1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                if(txtcall.getText().trim()!="" && txtcarr.getText().trim()!="" && txtlgr.getText().trim()!="")
                {
                    int val1 = Integer.parseInt(txtcall.getText().trim());
                    int val2 = Integer.parseInt(txtcarr.getText().trim());
                    Direccion dir = new Direccion( txtlgr.getText().trim(), val1, val2);
                    agente.buscarRuta(dir);
                }else{
                    msg("Debe rellenar todos los campos");
                }
            }
        });
    }

    public void msg(String mensaje) 
    {
        JOptionPane.showMessageDialog(this, mensaje);
    }
    public void aviso(String mensaje, String titulo) 
    {
        JOptionPane.showMessageDialog(this, mensaje, titulo, 2);
    }
}
