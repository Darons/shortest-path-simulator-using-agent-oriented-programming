/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import modelos.DatosPedido;
import modelos.Direccion;

/**
 *
 * @author NoHearth
 */
public class VResult extends JFrame
{
    private int cont;
    private float lista;
    private Container c;
    private JLabel dist1;
    private JLabel dist2;
    private JLabel titulo1;
    private JLabel titulo2;
    private JButton btnacp;
    private ModelTabla tabla1;
    private ModelTabla tabla2;
    private JScrollPane scroll1;
    private JScrollPane scroll2;
    private JTable tablaPedidos1;
    private JTable tablaPedidos2;
    private ArrayList<String> nombre;
    private ArrayList<Integer> calle;
    private ArrayList<Integer> carrera;
    
    public VResult(String rst1,String rst2, String ttl)
    {
        c = new Container();
        titulo2 = new JLabel("Ruta mas corta");
        titulo1 = new JLabel("Vecino mas cercano");
        btnacp =  new JButton("Aceptar");
        scroll1 =  new JScrollPane();
        scroll2 =  new JScrollPane();
        tablaPedidos1 = new JTable();
        tablaPedidos2 = new JTable();
        tabla1 =  new ModelTabla(new ArrayList<DatosPedido>());
        tabla2 =  new ModelTabla(new ArrayList<DatosPedido>());
        
        getDatos(rst1);
        dist1 = new JLabel("La distancia total recorrida es "+lista);
        for(int j=0;j<cont;j++)
        {
            Direccion direc = new Direccion(nombre.get(j),calle.get(j),carrera.get(j));
            tabla1.agregar_pedido(direc);
        }
        getDatos(rst2);
        dist2 = new JLabel("La distancia total recorrida es "+lista);
        for(int j=0;j<cont;j++)
        {
            Direccion direc = new Direccion(nombre.get(j),calle.get(j),carrera.get(j));
            tabla2.agregar_pedido(direc);
        }
        
        setBounds(0, 0, 790, 330);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle(ttl);
        setVisible(true);
        
        c = this.getContentPane();
        c.setLayout(null);
        c.setBackground(new Color(102, 102, 102));
        
        titulo1.setBounds(10, 10, 200, 28);
        titulo1.setFont(new Font("Cambria", 1, 12));
        titulo1.setBackground(new Color(102, 102, 102));
        titulo2.setBounds(400, 10, 200, 28);
        titulo2.setFont(new Font("Cambria", 1, 12));
        titulo2.setBackground(new Color(102, 102, 102));
        dist1.setBounds(10, 229, 250, 28);
        dist1.setFont(new Font("Cambria", 1, 12));
        dist1.setBackground(new Color(102, 102, 102));
        dist2.setBounds(400, 229, 250, 28);
        dist2.setFont(new Font("Cambria", 1, 12));
        dist2.setBackground(new Color(102, 102, 102));
        
        tablaPedidos1.setModel(tabla1);
        tablaPedidos1.setRowSelectionAllowed(true);
        tablaPedidos1.setColumnSelectionAllowed(false);
        tablaPedidos1.setFont(new Font("Cambria", 0, 12));
        tablaPedidos1.setBackground(new Color(204, 204, 204));
        tablaPedidos1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tablaPedidos1.getTableHeader().setResizingAllowed(false);
        tablaPedidos1.getTableHeader().setReorderingAllowed(false);
        
        tablaPedidos2.setModel(tabla2);
        tablaPedidos2.setRowSelectionAllowed(true);
        tablaPedidos2.setColumnSelectionAllowed(false);
        tablaPedidos2.setFont(new Font("Cambria", 0, 12));
        tablaPedidos2.setBackground(new Color(204, 204, 204));
        tablaPedidos2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tablaPedidos2.getTableHeader().setResizingAllowed(false);
        tablaPedidos2.getTableHeader().setReorderingAllowed(false);
        
        scroll1.setBackground(new Color(204, 204, 204));
        scroll1.setBounds(10, 58, 375, 150);
        scroll1.setViewportView(tablaPedidos1);
        scroll1.setFont(new Font("Cambria", 0, 12));
        
        scroll2.setBackground(new Color(204, 204, 204));
        scroll2.setBounds(400, 58, 375, 150);
        scroll2.setViewportView(tablaPedidos2);
        scroll2.setFont(new Font("Cambria", 0, 12));

        btnacp.setBounds(354, 259, 82, 28);
        btnacp.setFont(new Font("Cambria", 1, 12));
        btnacp.setBackground(new Color(102, 102, 102));
        
        c.add(titulo1);
        c.add(titulo2);
        c.add(scroll1);
        c.add(scroll2);
        c.add(btnacp);
        c.add(dist1);
        c.add(dist2);
        
        btnacp.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                dispose();
            }
        });
    }
    
    public void getDatos(String s)
    {
        calle = new ArrayList<>();
        nombre = new ArrayList<>();
        carrera = new ArrayList<>();
        String[] parts = s.split("\\+");
        String[] arreglo = parts[1].split("\\,");
        lista = Float.valueOf(parts[0]);
        cont = arreglo.length;
        System.out.println("Lista: "+Arrays.toString(arreglo));
        System.out.println("Distancia Total: "+lista +" Cont: "+cont);
        for(int i=0;i<cont;i++)
        {
            String[] pedidos = arreglo[i].split("-");
            nombre.add(pedidos[0]);
            calle.add(Integer.parseInt(pedidos[1]));
            carrera.add(Integer.parseInt(pedidos[2]));
        }
    }
}
