/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.awt.*;
import java.util.*;
import javax.swing.table.AbstractTableModel;
import modelos.DatosPedido;
import modelos.Direccion;

/**
 *
 * @author NoHearth
 */
public class ModelTabla extends AbstractTableModel
{
    private ArrayList<DatosPedido> listaPedidos;
    private String[] columnas = {"Id. del Pedido", "Direccion"};
    
    ModelTabla(ArrayList<DatosPedido> arrayList){
        super();
        this.listaPedidos = arrayList;
    }
    
    public String getColumnName(int col) {
        return columnas[col];
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public int getRowCount() {
        return listaPedidos.size();
    }

    @Override
    public Object getValueAt(int row, int col) {
        Object object = null;
        switch(col) {
        case 0:
            object = (Object) listaPedidos.get(row).getNombre();
            break;
        case 1:
            object = (Object) listaPedidos.get(row).getDireccion();
            break;
        }
        return object;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void agregar_pedido(Direccion dir) {
        DatosPedido direc = new DatosPedido(dir.getNombre(), dir.getDireccion());
        listaPedidos.add(direc);
        fireTableDataChanged();
    }

    public DatosPedido get_pedido(int index) {
        return listaPedidos.get(index);
    }

    public void actualizar_pedido(int index, Direccion dir_act) {
        DatosPedido direc = new DatosPedido(dir_act.getNombre(), dir_act.getDireccion());
        listaPedidos.set(index, direc);
        fireTableDataChanged();
    }

    public void eliminar_pedido(int index) {
        listaPedidos.remove(index);
        fireTableDataChanged();
    }

    public ArrayList<DatosPedido> get_pedidos() {
        return listaPedidos;
    }
}
