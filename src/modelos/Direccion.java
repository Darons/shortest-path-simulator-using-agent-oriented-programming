/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author NoHearth
 */
public class Direccion 
{
    private String nombre;
    private int numCalle = 0;  
    private int numCarrera = 0;

    public Direccion(String nm, int n, int nc) {
        this.nombre = nm;
        this.numCalle = n;
        this.numCarrera = nc;
    }

    public Direccion(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public int getNumCalle() {
        return numCalle;
    }

    public void setNumCalle(int n) {
        this.numCalle = n;
    }

    public int getNumCarrera() {
        return numCarrera;
    }

    public void setNumCarrera(int nc) {
        this.numCarrera = nc;
    }
    
    public String getDireccion()
    {
        return "Calle: "+getNumCalle()+" con Carrera: "+getNumCarrera();
    }
}
