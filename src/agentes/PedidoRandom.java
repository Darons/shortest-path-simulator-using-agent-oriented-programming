/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agentes;

import modelos.Direccion;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetResponder;
import java.util.ArrayList;
import vistas.ModelTabla;
import vistas.VPedidoRandom;

/**
 *
 * @author NoHearth
 */
public class PedidoRandom extends Agent
{
    int num;
    float dist;
    String cl;
    String crr;
    String nom;
    Direccion dir;
    ModelTabla tabla;
    VPedidoRandom vp;
    ArrayList<String> nombre;
    ArrayList<Integer> calle;
    ArrayList<Integer> carrera;
    
    protected void setup()
    {
        vp = new VPedidoRandom(this);
        vp.setVisible(true);
        nombre = new ArrayList<>();
        calle = new ArrayList<>();
        carrera = new ArrayList<>();
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());
        
        ServiceDescription sd = new ServiceDescription();       
        sd.setType("Pedido");
        sd.setName(this.getLocalName());
        dfd.addServices(sd);
        
        try{
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        
        MessageTemplate template = MessageTemplate.and(
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
                MessageTemplate.MatchPerformative(ACLMessage.CFP)
        );
        addBehaviour(new ContractNetResponder(this, template) 
        {
            protected ACLMessage handleCfp(ACLMessage cfp) throws NotUnderstoodException, RefuseException
            {
                int cantPed = Integer.valueOf(cfp.getContent());
                
                int lista = vp.getRow();
                String dato = vp.getDirecciones() +"+"+ lista;
                if(lista > cantPed)
                {
                    ACLMessage propose = cfp.createReply();
                    propose.setPerformative(ACLMessage.PROPOSE);
                    propose.setContent(dato);
                    return propose;
                }
                else if(lista == cantPed)
                {
                    ACLMessage propose = cfp.createReply();
                    propose.setPerformative(ACLMessage.FAILURE);
                    propose.setContent(String.valueOf(""));
                    return propose;
                }
                else
                {
                    throw new RefuseException("No hay pedido");
                }
            }
            
            protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) throws FailureException 
            {
                ACLMessage inform = accept.createReply();
                inform.setPerformative(ACLMessage.INFORM);
                return inform;
            }
        });
    }

    protected void takeDown() {
        vp.dispose();
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {}
    }
    
    
    public void getDatos(String s)
    {
        String[] arreglo = s.split("\\,");
        for(int i=0;i<arreglo.length-1;i++)
        {
            String[] pedidos = arreglo[i].split("-");
            System.out.println("Nombre"+(i+1)+": "+pedidos[0]);
            System.out.println("Calle"+(i+1)+": "+pedidos[1]);
            System.out.println("Carrera"+(i+1)+": "+pedidos[2]);
            nombre.add(pedidos[0]);
            calle.add(Integer.parseInt(pedidos[1]));
            carrera.add(Integer.parseInt(pedidos[2]));
        }
    }
    public void calDistancia(ArrayList<Integer> x, ArrayList<Integer> y, ArrayList<String> z)
    {
        float r,r1,r2, viejo = 9000;
        int a,b,c,d;
        int tam1 = x.size();
        int tam2 = y.size();
        a = x.get(0);
        b = y.get(0);
        if(tam1==tam2)
            for(int i=0; i<tam1-1; i++)
            {
                c = x.get(i+1);
                d = y.get(i+1);
                r1=(float)Math.pow((a-c),2);
                r2=(float)Math.pow((b-d),2);
                r = (float)Math.sqrt((r1+r2));
                if(r<viejo)
                {
                    viejo = r;
                    nom = z.get(i+1);
                    cl = String.valueOf(c);
                    crr = String.valueOf(d);
                }
            }
        dist = viejo;
    }
}
