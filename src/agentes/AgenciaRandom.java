/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agentes;

import jade.core.*;
import modelos.*;
import vistas.*;
import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;
import java.util.*;
/**
 *
 * @author NoHearth
 */
public class AgenciaRandom extends Agent
{
    private int z;
    private int dato;
    private int lista;
    //private Vector cam;
    private VResult vr;
    private String acum1;
    private String acum2;
    private String total1;
    private String total2;
    private String camino;
    private float[][] mat;
    private float distancia;
    private VAgenciaRandom va;
    private ArrayList<String> cam;
    private ArrayList<Integer> val;
    private ArrayList<String> nombre;
    private ArrayList<Integer> calle;
    private ArrayList<Integer> carrera;
    private ArrayList<String> ListaPedidos;
    
    protected void setup()
    {
        va = new VAgenciaRandom(this);
        va.setVisible(true);
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());
        
        ServiceDescription sd = new ServiceDescription();       
        sd.setType("Agencia");
        sd.setName(this.getLocalName());
        dfd.addServices(sd);
        
        try{
            DFService.register(this, dfd);
        } catch (FIPAException e) {System.out.println("Error; "+e);}
    }
    
    protected void takeDown() {
        va.dispose();
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {System.out.println("Error; "+e);}
    }
    
    public ArrayList<String> buscarPedidos()
    {
        ArrayList<String> pedido = new ArrayList<>();
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Pedido");
        dfd.addServices(sd);
        try {
            DFAgentDescription[] result = DFService.search(this, dfd);
            for(int i=0; i<result.length; i++) {
                pedido.add(result[i].getName().getLocalName());
            }
        } catch (FIPAException e) {
            System.out.println("Error; "+e);
        }
        return pedido;
    }
    
    public void buscarRuta(Direccion dr)
    {
        dato = 1;
        acum1 = "";
        acum2 = "";
        total1 = "";
        total2 = "";
        camino = "1";
        distancia = 0;
        cam = new ArrayList<>();
        calle = new ArrayList<>();
        nombre = new ArrayList<>();
        carrera = new ArrayList<>();
        ListaPedidos = buscarPedidos();
        ACLMessage msg = new ACLMessage(ACLMessage.CFP);
        Iterator<String> it = ListaPedidos.iterator();
        while(it.hasNext()) {
            msg.addReceiver(new AID(it.next(), AID.ISLOCALNAME));
        }
        msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
        msg.setReplyByDate(new Date(System.currentTimeMillis() + 5000));
        msg.setContent("0");
        nombre.add(dr.getNombre());
        calle.add(dr.getNumCalle());
        carrera.add(dr.getNumCarrera());
        
        addBehaviour(new ContractNetInitiator(this, msg)
        {
            protected void handleAllResponses(Vector responses, Vector acceptances) 
            {
                if(responses.size() > 0) 
                {
                    ACLMessage accept = null;
                    Enumeration e = responses.elements();
                    System.out.println("Tamaño f "+ acceptances.size());
                    while(e.hasMoreElements()) 
                    {
                        ACLMessage response = (ACLMessage) e.nextElement();
                        if (response.getPerformative() == ACLMessage.PROPOSE) 
                        {
                            ACLMessage reply = response.createReply();
                            reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                            acceptances.addElement(reply);
                            getDatos(response.getContent());
                            dato = dato + lista;
                            accept = reply;
                        }
                    }
                    if(accept != null) 
                    {
                        Matriz(dato, calle, carrera);
                        VecinoCercano(dato);
                        calRutas(0,0,dato,camino);
                        RutaCorta();
                        accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                    }else{
                    if(acceptances.isEmpty())
                        va.aviso("No se consiguieron Pedios","Error");
                    }
                }
                else
                    va.aviso("No hubo respuesta","Error");
            }
            protected void handleInform(ACLMessage inform) 
            {
                vr = new VResult(total1, total2, "Resultado-Listas de pedido");
            }
        });
    }
    
    public void Matriz(int n,ArrayList<Integer> cl, ArrayList<Integer> cr)
    {
        System.out.println("==============================");
        System.out.println("Calle: "+cl);
        System.out.println("Carrera: "+cr);
        System.out.println("Tamaño: "+ n);
        System.out.println("==============================");
	mat = new float[n][n];
	for (int i=0;i<n;i++)
            for(int j=0;j<n;j++)
            {
                mat[i][j] = getDistancia(cl.get(i), cr.get(i), cl.get(j), cr.get(j));
    	    }
    }
    
    public float getDistancia(int i, int j, int k, int l)
    {
        int a,b;
        float r1,r2;
	a = i - k;
	b = j - l;
        r1=(float)Math.pow(a,2);
        r2=(float)Math.pow(b,2);
    	return (float)Math.sqrt((r1+r2));
    }
    
    public void getDatos(String s)
    {
        String[] parts = s.split("\\+");
        String[] arreglo = parts[0].split("\\,");
        lista = Integer.valueOf(parts[1]);
        
        for(int i=0;i<arreglo.length-1;i++)
        {
            String[] pedidos = arreglo[i].split("-");
            nombre.add(pedidos[0].trim());
            calle.add(Integer.parseInt(pedidos[1].trim()));
            carrera.add(Integer.parseInt(pedidos[2].trim()));
        }
    }
    public void calRutas(int p, int i, int n, String cadena)
    {
        if (p<n && i< n)
        {
            if(cadena.startsWith("1"))
            {
                boolean no=false;
                if(cadena.contains("-"))
                {
                    String[] temp = cadena.split("-");
                    val = new ArrayList<>();
                    for(int m=0;m<temp.length;m++)
                        val.add(Integer.valueOf(temp[m]));                    
                }
                else{
                    val = new ArrayList<>();
                    val.add(Integer.valueOf(cadena));
                }
                for(int k=0; k<val.size() && !no; k++)
                {
                    if(val.get(k) == i+1)
                        no=true;
                }
                if(no==false) 
                {
                    calRutas(p+1,0,n,cadena+"-"+(i+1));
                }
                calRutas(p,i+1,n,cadena);   
                if(n>9)
                {
                    z = 9 + (n-1) + (n-9)*2;
                }else{
                    z = n + n-1;
                }
                if(cadena.length() == z)
                {
                    cadena=cadena+"-"+1;
                    if(!cam.contains(cadena))
                    {
                        cam.add(cadena);
                        System.out.println("Acumulado: "+cadena);
                    }
                }
            }
        }
    }
    
    public void RutaCorta()
    {
        String sig ="";
        float menor = calDist(cam.get(0)), m;
        for(int i=0;i<cam.size();i++)
        {
            
            m = calDist(cam.get(i));
            if( m< menor)
            {
                menor= m;
                sig = cam.get(i);
            }
            System.out.println("Nueva Ruta: "+menor);
            System.out.println("Camino a seguir: "+sig);
        }
        String[] last = sig.split("-");
        for(int b=0;b<last.length-1;b++)
        {
            acum2 = acum2 + nombre.get(Integer.parseInt(last[b])-1) + "-"
                    + calle.get(Integer.parseInt(last[b])-1) + "-"
                    + carrera.get(Integer.parseInt(last[b])-1) + ",";
        }
        acum2 = acum2 + nombre.get(0) + "-" + calle.get(0) + "-" + carrera.get(0); 
        total2 = menor + "+" + acum2;
    }

    public float calDist(String camino)
    {
        String[] ruta = camino.split("-");
        float num=0;
	int a,b;
        for (int i=0;i<ruta.length-1;i++) 
        {
            a=Integer.parseInt(ruta[i]);
            b=Integer.parseInt(ruta[i+1]);
            num=num+mat[a-1][b-1];   	  
        }
        return num;
    }
    
    public void VecinoCercano(int t)
    {
        String[] direc = new String[t];
        String[] dist = new String[t];
        boolean op, val = false;
        float act, viejo, ini;
        int va = -1, p, q = 0, r;
        
        for(int i=0;i<t-1;i++){
            op = false;
            viejo=9000;
            if(i==0){
                direc[i] = String.valueOf(1);
                dist[i] = String.valueOf(i);
                va = i;
            }
            for(int j=0;j<t;j++){
                act = mat[va][j];
                System.out.println("Actual: "+act);
                for(int k=0;k<i+1;k++)
                {
                    ini = mat[va][0];
                    r = Integer.parseInt(direc[k]);
                    if(act==ini && r==j){
                        op=false;
                        break;
                    }else{
                        op=true;
                    }
                }
                if(op){
                    System.out.println("Viejo: "+viejo);
                    if(act<=viejo && act!=0){
                        p=j+1;
                        for(int n=0;n<i+1;n++){   
                            float m = Float.valueOf(direc[n]);
                            if(m==p){
                                val = false;
                                break;
                            }
                            else{
                                val = true;
                            }
                        }
                        if(val){
                            direc[i+1] = String.valueOf(p);
                            System.out.println("Direccion: "+p);
                            viejo = act;
                            dist[i+1] = String.valueOf(viejo);
                            q = p;
                        }                            
                    }
                }
            }
            va = q-1;
            System.out.println("Fin Iteracion "+i);
            System.out.println("Lista: "+Arrays.toString(direc));
            System.out.println("Distancias: "+Arrays.toString(dist));
            System.out.println("Nuevo n: "+va);
            System.out.println("==============================");
        }
        int a = 0,b = 0;
        for (int l=0;l<direc.length;l++)
        {
            acum1 = acum1 + nombre.get(Integer.parseInt(direc[l])-1) + "-"
                    + calle.get(Integer.parseInt(direc[l])-1) + "-"
                    + carrera.get(Integer.parseInt(direc[l])-1) + ",";
            distancia = distancia + Float.parseFloat(dist[l]);
            a = calle.get(Integer.parseInt(direc[l])-1);
            b = carrera.get(Integer.parseInt(direc[l])-1);
        }
        System.out.println("a: "+a);
        System.out.println("b: "+b);
        acum1 = acum1 + nombre.get(0) + "-" + calle.get(0) + "-" + carrera.get(0); 
        distancia = distancia + getDistancia(calle.get(0), carrera.get(0), a, b);
        total1 = distancia + "+" + acum1;
    }
}
